"""Guillermo del Saz"""
class Order:

    def __init__(self, id_order: int, customer: str, raw_material: str, min_quality_level: int, min_height: float,
                 max_height: float, min_quantity: int, max_quantity: int, min_cut_length: float, max_cut_length: float):
        self.id_order = id_order
        self.customer = customer
        self.raw_material = raw_material
        self.min_quality_level = min_quality_level
        self.min_height = min_height
        self.max_height = max_height
        self.min_quantity = min_quantity
        self.max_quantity = max_quantity
        self.min_cut_length = min_cut_length
        self.max_cut_length = max_cut_length

        self.assigned_stock_pieces = {}

    """ Sumas el area que ocupa una order, es decir cuanta tela para dicho order"""
    def get_assigned_quantity(self) -> float:
        suma = 0
        for obj in self.assigned_stock_pieces:
            suma += obj.get_area()
        return suma


class StockPiece:

    def __init__(self, id_stock_piece: int, height: float, length: float, quality_level: int, position_x: float,
                 position_y: float, id_father_stock_piece: int):
        self.id_stock_piece = id_stock_piece
        self.height = height
        self.length = length
        self.quality_level = quality_level
        self.position_x = position_x
        self.position_y = position_y
        self.id_father_stock_piece = id_father_stock_piece

        self.assigned_order = 0

    def get_area(self) -> float:
        """Devuelve el area"""
        return self.height * self.length

    def assign_order(self, order: Order):
        """Asigna el stockpiece al order y viceversa"""
        self.assigned_order = order
        order.assigned_stock_pieces[order] = self
        return self.assigned_order, order.assigned_stock_pieces[order]


class Map:
    def __init__(self, maptype: str, real: int):
        self.maptype = maptype
        self.real = real

        self.assigned_stockpieces = set([])
        self.assigned_fabric_sheet = set([])

    def get_assigned_quantity(self) -> float:
        """ Nos devuelve la suma de los areas de las stockpieces del mapa"""
        suma = 0
        for obj in self.assigned_stockpieces:
            suma += obj.get_area()
        return suma

    def add_stock_piece(self, sp: StockPiece):
        """ Asigna el stockpiece al mapa y viceversa"""
        return self.assigned_stockpieces.add(sp)


class FabricSheet:

    def __init__(self, id_fabric_sheet: int, raw_material: str, height: float, length: float, department: str,
                 weaving_forecast: str):
        self.id_fabric_sheet = id_fabric_sheet
        self.raw_material = raw_material
        self.height = height
        self.length = length
        self.department = department
        self.weaving_forecast = weaving_forecast

        self.assigned_maps = set([])
"""Aquí debería crear los mapas"""

def add_map(self, map1: Map):
    """ Aqui asignaria los mapas (principio,intermedio,final) a cada fabric sheet y viceversa"""
    return self.assigned_maps.add(map1), map1.assigned_fabric_sheet.add(self)

""" Me estaba dando error en el import."""
ordenpri = Order("1", "Scalpers", "Lino", "5", 5, 8, 5, 6, 3, 4)

stock1 = StockPiece(1, 2, 5, "6", 3, 2, 4)
stock2 = StockPiece(2, 3, 4, "6", 4, 3, 3)

stock1.assign_order(ordenpri)
stock2.assign_order(ordenpri)

Material1 = FabricSheet("1", "Lino", 15, 15, "Dep1", 1)

Suma = ordenpri.get_assigned_quantity()