import numpy as np
import multiprocessing as mp
import json
from itertools import repeat


pyd_dict = {
    70: 40,
    80: 34,
    90: 30,
    100: 27,
    110: 24,
    120: 22,
    140: 18,
    160: 15
}


def c_rev (p: int, r: int, d: int):
    """

    :param p: precio
    :param r: habitaciones
    :param d: demanda
    :return: El minimo entre la demanda y el precio ya que es todas
    las habitaciones que queden o tod lo que se pueda vender
    """
    return min(d, r) * p


def montecarlo_friday(rooms: int):
    """

    :param rooms: habitaciones
    :return: devuelve el maximo valor de revenue para la demanda
    """
    n_iter = 50000
    rev = {}
    for i in pyd_dict.keys():
        lamb = pyd_dict.get(i)
        demand = np.random.poisson(lamb, n_iter)
        var = repeat(rooms, n_iter)
        revenue = map(c_rev, repeat(i, n_iter), var, demand)
        f_rev = sum(revenue) / n_iter
        rev.update({i: f_rev})
    return max(rev, key=rev.get)


def friday_price(mc: callable) -> list:
    """

    :param mc: La funcion montecarlo
    :return:  Devuelve la lista con los valores retornados por la funcion
    montecarlo para cada num de habitaciones
    """
    with mp.Pool(4) as hilos:  # introduzco los hilos para ejecutar
        pr = hilos.map(mc, range(0, 71))
        # el rango es para el num de habitacion
    return pr


if __name__ == '__main__':
    price = friday_price(montecarlo_friday)
    index = []
    for i in range(0, 71):  # creo la lista del indice para el fichero
        index.append(i)

    friday_prices = dict(zip(index, price))  # combino las listas
    print(friday_prices)
    # https://stackoverflow.com/questions/7271385/
    # how-do-i-combine-two-lists-into-a-dictionary-in-python
    with open('Fridayjson.json', 'w') as file:  # los saco al fichero
        json.dump(friday_prices, file, sort_keys=True, indent=4)
