from email_validator import validate_email, EmailNotValidError
import sqlite3
import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()
conn = sqlite3.connect('Usuario.db')

"""Class is created with the attributes, id must me unique"""
conn.execute('''CREATE TABLE if not exists Usuarios 
             (id INTEGER NOT NULL, name text, surname text, 
             mail text, phone1 text,phone2 text, age integer 
             , PRIMARY KEY (id))''')

"""Creo función Remove para limpiar los contenidos de la tabla y 
   que los insert no se Ejectuten en cada compilación"""


def remove_emp():
    with conn:
        conn.execute("DELETE from Usuarios")


remove_emp()


class Mailval:
    """ I chech that the syntax is correct and use validate_email"""

    def __set_name__(self, owner, name):
        """

        :param owner:
        :param name:
        :return:
        """
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, owner):
        """

        :param obj:
        :param owner:
        :return:
        """
        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        """

        :param obj:
        :param value:
        :return:
        """
        try:

            validate_email(value)
            conn.execute(self.store, [value, obj.key])
            conn.commit()

        except EmailNotValidError or SyntaxError:

            raise ValueError("Not valid email")


class Ageval:
    """I check that the age is an integer and that it is valid"""

    def __set_name__(self, owner, name):
        """

        :param owner:
        :param name:
        :return:
        """
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, owner):

        """

        :param obj:
        :param owner:
        :return:
        """
        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        """

        :param obj:
        :param value:
        :return:
        """
        res = isinstance(value, int)
        if not res:
            raise ValueError("age must be an integer")
        if value < 0 or value > 120:
            raise ValueError("age must be  <120 o >0")
        else:
            conn.execute(self.store, [value, obj.key])
            conn.commit()


class Phoneval:
    """I check the fist value for a +, that the rest are decimal
                    and if the length is correct"""

    def __set_name__(self, owner, name):
        """

        :param owner:
        :param name:
        :return:
        """
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, owner):
        """

        :param obj:
        :param owner:
        :return:
        """

        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        """

        :param obj:
        :param value:
        :return:
        """
        numbers = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')

        if value.find("+") != 0:
            raise ValueError("Phone must contain a + at the start")
        elif len(value) < 9 or len(value) > 11:
            raise ValueError("Phone must have 9/10/11 digits")
        for i in range(1, len(value)):
            if value[i] not in numbers:
                raise ValueError("Phone must not contain char")
        else:
            conn.execute(self.store, [value, obj.key])
            conn.commit()


class Prueb:
    """this is class used by values that do not need validation"""
    def __set_name__(self, owner, name):
        """

        :param owner:
        :param name:
        :return:
        """
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, owner):
        """

        :param obj:
        :param owner:
        :return:
        """
        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        """

        :param obj:
        :param value:
        :return:
        """
        conn.execute(self.store, [value, obj.key])
        conn.commit()


class User:
    """this is the main class. values are stored"""
    table = "Usuarios"
    key = "id"
    mail = Mailval()
    name = Prueb()
    surname = Prueb()
    phone1 = Phoneval()
    phone2 = Phoneval()
    age = Ageval()

    def __init__(self, key):
        """

        :param key:
        """
        self.key = key


conn.commit()

conn.execute("INSERT INTO Usuarios VALUES (1, 'Juan', 'pan',"
             " 'juanpan@mydomain.com', '+123456789', '+987654321', 12)")

conn.execute("INSERT INTO Usuarios VALUES (2, 'Jose', 'pon',"
             " 'Josepon@mydomain.com', '+987654321', '+123456789', 13)")

conn.execute("INSERT INTO Usuarios VALUES (3, 'Javi', 'pen',"
             " 'Javipen@mydomain.com', '+123456789', '+987654321', 14)")

conn.commit()

"""I create this classes with the id"""
Juan = User(1)
Jose = User(2)
Javi = User(3)

try:
    assert Juan.age == 12

except AssertionError:
    print("Assertion error 1")

try:
    Juan.age = 40

except ValueError:
    print("Value Error: inappropriate age value")

try:
    assert Juan.age == 40
except AssertionError:
    print("Assertion error 2")

try:
    Juan.age = 200
except ValueError:
    print("Value Error: inappropriate age value")

try:
    assert Juan.phone1 == "+123456789"

except AssertionError:
    print("Assertion error 3")

try:
    Juan.phone1 = "+123456779"

except ValueError:
    print("Value Error: inappropriate phone value")

try:
    assert Juan.phone1 == "+123456779"
except AssertionError:
    print("Assertion error 4")

try:
    Juan.phone1 = "44"
except ValueError:
    print("Value Error: inappropriate phone value")

try:
    assert Juan.mail == "juanpan@mydomain.com"

except AssertionError:
    print("Assertion error 5")

try:
    Juan.mail = "juanpana@mydomain.com"

except ValueError:
    logger.error("Value Error: inappropriate mail value")

try:
    assert Juan.mail == "juanpana@mydomain.com"
except AssertionError:
    print("Assertion error 6")

try:
    Juan.mail = "r.vazquez@uf"
except ValueError:
    logger.error("Value Error: inappropriate mail value")


conn.commit()

conn.close()
